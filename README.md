# VE303 phase 1 study in healthy volunteers

## Reference

Dsouza M, Menon R, et al. "Engraftment of the Live Biotherapeutic Product VE303 in Healthy Volunteers and Associations with the Resident Microbiota and Metabolites" (submitted)

Dsouza et al. describe VE303, a live biotherapeutic product developed for the treatment of CDI. A phase 1 study in healthy volunteers found that VE303 was safe and well tolerated. Dosing VE303 daily for up to 14 days after vancomycin led to rapid and durable VE303 strain engraftment. 

**Highlights**
- The VE303 LBP is effective at treating C. difficile infection (CDI) in mice
- In a phase 1, dose escalation study, VE303 was safe and well-tolerated
- Microbiota depletion and multi-days of dosing lead to VE303 engraftment to 1 year
- Higher VE303 doses were associated with microbiota and metabolite changes


## Analysis summary

**Scripts used**
- VE303_logisticAUC.Rmd - fit VE303 strain colonization to logistic model; calculate AUC

- VE303_spike_in_recovery.Rmd - colonization assay validation
