---
title: "Spike_in_validation"
author: "E. Crossette"
date: "7/22/2021"
output: html_document
---

# Load Environment
Change this to source the r script with library list and pretty plot settings

```{r Environment Setup, include=FALSE, warning=FALSE}
knitr::opts_chunk$set(echo = FALSE)

source("R_scripts/libraries.R")

strain.cols.303<- c("VE303-01" = "#1b9e77", "VE303-02" = "#d95f02", "VE303-03" = "#7570b3", "VE303-04" = "#e7298a", "VE303-05" = "#66a61e", "VE303-06" = "#e6ab02", "VE303-07" = "#a6761d", "VE303-08" = "#666666")

```

### Sample Summary
2. 3 Master Stool samples
    + 2 VE303 Spike ins (high and low)
    + 1 No Spike in
3. 1 LBP Product
    + VE303

## File Paths
```{r Define file pathds}

OC_names = "../../../OneDrive - Vedanta Biosciences/OneCodex_Sequencing_Pilot/metadata/OneCodexReadnames.csv"
oc_303 = "../../../OneDrive - Vedanta Biosciences/OneCodex_Sequencing_Pilot/markerpanels/VE303_Ph2_Extended_Marker_Data_20210505.csv"

```

```{r Observed to expected abundances of spike-ins}

# Sample name Key
OC_names = read.csv(OC_names)

# Load VE303 marker panel samples
oc_303 = read.csv(oc_303)
oc_303$Sample = gsub(".fastq.gz", "", oc_303$filename)
oc_303 = merge(OC_names[c(2,1)], oc_303)

# Set shorthand Names
short_names = data.frame(
  Name = c("COVUK0026_HighSpike_303", "COVUK0026_LowSpike_303", "COVUK0026_NoSpike", "VE303_LBP"),
  shorthand = c("10% VE303", "0.1% VE303", "No VE303", "100% VE303")
)

spikein = merge(short_names, oc_303[-c(4:12)])

```


```{r Compute how different strain abundances are to expected}

observed_expected = spikein[c(1,2,4,16,20)]%>%
  mutate(expected_rel_abund = 
         case_when(
             shorthand=="100% VE303" ~100/8/100,
             shorthand=="10% VE303" ~ 10/8/100,
             shorthand=="0.1% VE303" ~ 0.1/8/100,
             shorthand=="No VE303" ~ 0),
         diff = expected_rel_abund-targeted_panel_relative_abundance,
         percent_diff = (diff/expected_rel_abund)*100,
         fold_diff = targeted_panel_relative_abundance/expected_rel_abund,
         expected_observer = 1/fold_diff,
         LFC = log10(1/fold_diff))

```


```{r - PLot for VE303Ph1 relative abundances by spike-in Percent}


observed_expected$factor_order = factor(observed_expected$shorthand, levels=c("100% VE303","10% VE303","0.1% VE303","No VE303"))

#spike_in_boxplot = 
  ggplot(observed_expected, aes(factor_order, targeted_panel_relative_abundance))+
  geom_boxplot() +
  geom_jitter(aes(color = organism, shape = detection_status), width = 0.25, size = 2) +
  geom_hline(yintercept = c(0.125, 0.0125, 0.000125), linetype = "dashed") +
  annotate("text",
           x = c(3, 3, 1.5),
           y = c(0.135, 0.0135, 0.000135),
           label = c("Expected strain abundance,\n 100% VE303", 
                     "Expected strain abundance,\n 10% VE303", 
                     "Expected strain abundance,\n 0.1% VE303"),
           family = "", fontface = 3, size=4) + 
  scale_color_manual(values = strain.cols.303) +
  labs(y = "Percent Relative Abundance",
       x = "Percent of VE303 Spike-in",
       color = "VE303 Strain",
       shape = "Detection Status") +
  scale_y_log10(labels = scales::percent, breaks = c(0.125, 0.0125, 0.00125, 0.000125)) +
  theme(
    panel.border = element_rect(color="grey30", fill=NA),
    panel.background = element_rect(color="grey30", fill="white"),
    strip.background = element_rect(color="grey30"),
    panel.grid.major = element_line(color="grey93")

  )

ggsave(spike_in_boxplot, filename = "figures/spikein_vs_expected_boxplot.jpeg", height = 4.5,  width= 6.5, units = "in", dpi = 600)

```




```{r Plot!}

P_spike = 
  ggplot(subset(spike_plot, relative_abundance>0),
                aes(x = abundance_metric))+
  facet_wrap(~factor_order, nrow = 1) +
  labs(y = "LBP Strains Detection") +
  guides(x= guide_axis(angle=-45)) +
  scale_x_discrete(limits = positions) +
  geom_bar(stat = "count", aes(fill=organism, color = detection_status, alpha = detection_status, linetype = detection_status), size = 1) +
  scale_alpha_manual(values = c(1, 0.75, 1, 0.1)) +
    scale_color_manual(values = c("black", "orange", NA, "red")) +
    # scale_y_continuous(labels = scales::percent) +
  scale_fill_manual(values = strain.cols.303) +
  pretty_plot +
  theme(axis.title.x = element_blank())

ggsave(P_spike, filename = "figures/spikein_vs_expected_strain_counts.jpeg", height = 4.5,  width= 11, units = "in")


P_spike_rel_abund = 
  ggplot(subset(spike_plot, relative_abundance>0),
                aes(x = abundance_metric, y = relative_abundance))+
  facet_wrap(~factor_order, nrow = 1, scales = "free_y") +
  labs(y = "LBP Strain Relative abundance") +
  guides(x= guide_axis(angle=-45)) +
  scale_x_discrete(limits = positions) +
  geom_bar(stat = "identity", aes(fill=organism, color = detection_status, alpha = detection_status, linetype = detection_status), size = 1) +
  scale_alpha_manual(values = c(1, 0.75, 1, 0.1)) +
    scale_color_manual(values = c("black", "orange", NA, "red")) +
    scale_y_continuous(labels = scales::percent) +
  scale_fill_manual(values = strain.cols.303) +
  pretty_plot +
  theme(axis.title.x = element_blank())

P_spike_rel_abund

ggsave(P_spike_rel_abund, filename = "figures/spikein_vs_expected_rel_abund.jpeg", height = 4.5,  width= 11, units = "in")
```

